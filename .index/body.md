# Index

::: script
sort = parameters.get("sort") || "updated-at:desc"
pages = sam.search("*", parameters.get("sort") || "updated-at:desc")
:::

{{ function printSortLink(field, order) { }}{{? it.sort === field + ":" + order }}{{? order === "asc" }}￬{{??}}￪{{?}}{{??}}[{{= order === "asc" ? "￬" : "￪" }}](.index?sort={{= field }}:{{= order }}){{?}}{{ } }}

::: grid has-header is-striped
spans: {mobile: [6, 3, 3]}
-
Title {{ printSortLink("title", "asc"); }} {{ printSortLink("title", "desc"); }}
-
Last update {{ printSortLink("updated-at", "asc"); }} {{ printSortLink("updated-at", "desc"); }}
-
Last author {{ printSortLink("last-committer", "asc"); }} {{ printSortLink("last-committer", "desc"); }}
-
{{~ it.pages :page:index}}
[{{= page.title }}]({{= boka.serialize(page.reference) }})
-
{{= boka.formatTime(page.updatedAt) }}
-
{{= page.lastCommitter ? page.lastCommitter : "" }}
-
{{~}}
:::
